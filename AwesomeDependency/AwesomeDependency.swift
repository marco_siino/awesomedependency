//
//  AwesomeDependency.swift
//  AwesomeDependency
//
//  Created by Marco Siino on 21/01/2020.
//  Copyright © 2020 Marco Siino. All rights reserved.
//

import Foundation

public class AwesomeDependency {
    public init() { }
    
    public func awesomeMethod(str: String, doubleAwesome: Bool) -> String {
        if doubleAwesome {
            return "\(str), Awesooooooooomee!!! AWEEEEEEEEEEEEEEESSSSOMMMMEEEEEEEEEEEEEEE!!!!"
        }
        else {
            return "\(str), Awesooooooooomee!!!"
        }
    }
    
    public static func awesomeStaticMethod() -> String {
        return "Static Awesoooooomeeee!!!"
    }
}
