//
//  AwesomeDependency.h
//  AwesomeDependency
//
//  Created by Marco Siino on 21/01/2020.
//  Copyright © 2020 Marco Siino. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for AwesomeDependency.
FOUNDATION_EXPORT double AwesomeDependencyVersionNumber;

//! Project version string for AwesomeDependency.
FOUNDATION_EXPORT const unsigned char AwesomeDependencyVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AwesomeDependency/PublicHeader.h>


